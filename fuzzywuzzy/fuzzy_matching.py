# Importation des modules
import pandas as pd
from fuzzywuzzy import fuzz
from fuzzywuzzy import process


def data_matching():
    """
    Associe à chaque ligne du tableau en entrée le nom d'entreprise correspondant dans le tableau de référence
    (s'il existe une telle correspondance), et renvoie un nouveau tableau en sortie contenant cette information.

    /!\ ATTENTION /!\ : il faut veiller à indiquer le chemin vers le tableau d'entrée et de sortie,
    et indiquer la colonne du tableau d'entrée contenant le nom d'entreprise.
    (voir les premières lignes de la fonction)
    """
    ## /!\ INDIQUER LES CHEMINS VERS LES TABLEAUX D'ENTREE ET DE SORTIE /!\
    input_path = "./inputs/coal-tracker.csv"
    output_path = "./fuzzywuzzy/coal-tracker_fuzzy_matched.csv"

    ref_path = "./data/10_000_labelled_companies.csv"

    # Création des dataframes correspondant au tableau de référence
    # et au tableau d'entrée
    ref = pd.read_csv(ref_path, delimiter=",")
    raw = pd.read_csv(input_path, delimiter=",")

    # Insertion d'une colonne qui va contenir les noms unifiés des
    # entreprises et d'une colonne indiquant le score du matching

    ## /!\ INDIQUER L'INDEX DE LA COLONNE CONTENANT LES NOMS D'ENTREPRISES DANS LE TABLEAU D'ENTREE /!\
    ncol_rawname = 8

    ncol_ent, ncol_score = ncol_rawname + 1, ncol_rawname + 2
    raw.insert(ncol_ent, "entreprise", "None")
    raw.insert(ncol_score, "matching_score", "None")

    # Création de la liste des noms unifiés des entreprises
    ls_ref = ref["label"].tolist()
    ls_ref = list(set(ls_ref))  # suppression des doublons

    # Ajout du nom unifié de l'entreprise dans la nouvelle colonne
    # pour chaque ligne

    for i in range(len(raw)):
        current_name = str(raw.iloc[i, ncol_rawname]).lower()

        # On insère le meilleur matching
        raw.iloc[i, ncol_ent], raw.iloc[i, ncol_score] = process.extractOne(
            current_name, ls_ref
        )

        # On associe la valeur None si le score du matching est inférieur à 90
        if raw.iloc[i, ncol_score] < 90:
            raw.iloc[i, ncol_ent] = None

    # Création du fichier csv désiré
    raw.to_csv(output_path, index=False)


if __name__ == "__main__":
    data_matching()
