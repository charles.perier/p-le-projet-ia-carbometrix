import pandas as pd
import pandas_dedupe

ref = "./data/10_000_labelled_companies.csv"  # chemin du tableau de référence
raw = "./inputs/coal-tracker.csv"  # chemin du tableau d'entrée

if __name__ == "__main__":
    # Création des dataframes pandas à partir des fichiers csv
    df_clean = pd.read_csv(ref)[["label"]]
    df_messy = pd.read_csv(raw)

    # Formation du dataframe final avec dedupe
    df_final = pandas_dedupe.gazetteer_dataframe(
        df_clean, df_messy, field_properties="parent", canonicalize=True
    )
    # La valeur de field_properties correspond au nom de la colonne contenant les noms d'entreprises

    # Conversion du dataframe final en fichier csv
    df_final.to_csv("dedupe_output.csv")