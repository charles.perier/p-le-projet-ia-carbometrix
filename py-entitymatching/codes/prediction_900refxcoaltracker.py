"""
This file is used to predict the output on 900_ref x coal-tracker
You can choose the model
The current blocker is the overlap blocker but it can be modified
"""

import sys

sys.path.append(
    "/Users/pradap/Documents/Research/Python-Package/anhaid/py_entitymatching/"
)

import py_entitymatching as em
import pandas as pd
import os
import PyQt5
import random
import pickle

if __name__ == "__main__":
    pd.set_option("display.max_columns", None)

    rf = pickle.load(open("model2", "rb"))

    path_A = "../../data/900_ref.csv"
    path_B = "../../data/coal-tracker.csv"
    A = pd.read_csv(path_A)

    # I renamed A because I was not sure of how the parser works, but it is probably useless.
    A = A.rename(
        columns={
            "companyName": "company_name",
            "parentCompanyName": "parent_company_name",
        }
    )

    B = pd.read_csv(path_B)
    A["id"] = A.index
    em.set_key(A, "id")
    em.set_key(B, "id")
    em.to_csv_metadata(A, path_A)  # , key="parent_company_name")
    em.to_csv_metadata(B, path_B)  # , key="parent")

    print("Number of tuples in A: " + str(len(A)))
    print("Number of tuples in B: " + str(len(B)))
    print(
        "Number of tuples in A X B (i.e the cartesian product): " + str(len(A) * len(B))
    )

    print(A.keys())
    print(B.keys())

    ### DEFINING BLOCKERS

    # overlap blocker: same companies should have
    # common tokens in their names

    ob = em.OverlapBlocker()
    C = ob.block_tables(
        A,
        B,
        "company_name",
        "sponsor",
        overlap_size=1,
        l_output_attrs=["company_name", "parent_company_name"],
        r_output_attrs=["sponsor", "parent"],
        show_progress=False,
    )

    # print(len(C))
    # Rule-based blocker
    atypes1 = em.get_attr_types(A)
    atypes2 = em.get_attr_types(B)
    block_c = em.get_attr_corres(A, B)
    block_t = em.get_tokenizers_for_blocking()
    block_s = em.get_sim_funs_for_blocking()
    block_f = em.get_features(A, B, atypes1, atypes2, block_c, block_t, block_s)

    # Here we are going to explain how we can use a RuleBasedBlocker with a user defined feature.
    # The example I used is taken from here: http://anhaidgroup.github.io/py_entitymatching/v0.4.0/user_manual/create_feats_for_blocking.html#adding-removing-features
    # in the section "Adding a Blackbox Function as Feature".

    # Below is a custom example.
    # The name of the fields we want to compare in the feature.
    a_name = "companyName"
    b_name = "sponsor"

    # The function representing the feature (should return a number).
    def my_feature_function(ltuple, rtuple):
        a = random.uniform(0, 100)
        return a

    # Register this function in the block sim_funs.
    block_s["my_feature_function"] = my_feature_function

    # Creating a feature from the registered function.
    my_feature = em.get_feature_fn(
        f"my_feature_function(ltuple['{a_name}'], rtuple['{b_name}'])", block_t, block_s
    )
    # Adding the feature to the features block.
    em.add_feature(block_f, f"{a_name}_{b_name}_my_feature_function", my_feature)

    # Defining a rule base blocker.
    rb = em.RuleBasedBlocker()

    # Creating a ruleK.
    rule1 = ["companyName_sponsor_my_feature_function(ltuple, rtuple) > 90"]

    # Adding the rule to the blocker.
    rb.add_rule(rule1, block_f)

    
    # Generate features automatically
    feature_table = em.get_features_for_matching(
        A, B, validate_inferred_attr_types=False
    )

    # use the following line to find how to complete attrs_from_table
    # print(G.head())

    # Select the attrs. to be included in the feature vector table
    attrs_from_table = [
        "ltable_company_name",
        "ltable_parent_company_name",
        "rtable_sponsor",
        "rtable_parent",
    ]

    # Convert the cancidate set to feature vectors using the feature table
    L = em.extract_feature_vecs(
        C,
        feature_table=feature_table,
        attrs_before=attrs_from_table,
        show_progress=False,
        n_jobs=-1,
    )

    # Get the attributes to be excluded while predicting
    attrs_to_be_excluded = []
    attrs_to_be_excluded.extend(["_id", "ltable_id", "rtable_id"])
    attrs_to_be_excluded.extend(attrs_from_table)

    # Predict the matches
    predictions = rf.predict(
        table=L,
        exclude_attrs=attrs_to_be_excluded,
        append=True,
        target_attr="predicted",
        inplace=False,
    )

    # Get the attributes to be projected out
    attrs_proj = []
    attrs_proj.extend(["_id", "ltable_id", "rtable_id"])
    attrs_proj.extend(attrs_from_table)
    attrs_proj.append("predicted")

    # Project the attributes
    predictions = predictions[attrs_proj]

    predictions.to_csv("out2.csv", index=False)
    print(predictions.head())

    # HERE WE COULD USE TRIGGERS TO CORRECT THE OUTPUT
