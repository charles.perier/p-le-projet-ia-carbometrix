"""
This file is used to train and create the prediction models
The model is trained matching 900_ref.csv and coal-tracker.csv
"""

import sys

sys.path.append(
    "/Users/pradap/Documents/Research/Python-Package/anhaid/py_entitymatching/"
)

import py_entitymatching as em
import pandas as pd
import os
import PyQt5
import random
import pickle
from collections import Counter
from fuzzywuzzy import fuzz
from time import process_time


if __name__ == "__main__":
    pd.set_option("display.max_columns", None)

    t1_start = process_time()
    ### IMPORTING THE DATASETS

    # Get the paths
    # A = ref
    # B = coal-tracker

    path_A = "../../data/900_ref.csv"
    path_B = "../../data/coal-tracker.csv"
    A = pd.read_csv(path_A)

    # I renamed A because I was not sure of how the parser works, but it is probably useless.
    A = A.rename(
        columns={
            "companyName": "company_name",
            "parentCompanyName": "parent_company_name",
        }
    )

    B = pd.read_csv(path_B)
    A["id"] = A.index
    em.set_key(A, "id")
    em.set_key(B, "id")
    em.to_csv_metadata(A, path_A)  # , key="parent_company_name")
    em.to_csv_metadata(B, path_B)  # , key="parent")

    print("Number of tuples in A: " + str(len(A)))
    print("Number of tuples in B: " + str(len(B)))
    print(
        "Number of tuples in A X B (i.e the cartesian product): " + str(len(A) * len(B))
    )

    print(A.keys())
    print(B.keys())

    ### DEFINING BLOCKERS

    # overlap blocker: same companies should have
    # common tokens in their names

    # uncomment the following code if you want to use an overlap blocker

    """
    ob = em.OverlapBlocker()
    C = ob.block_tables(
        A,
        B,
        "company_name",
        "sponsor",
        overlap_size=1,
        l_output_attrs=["company_name", "parent_company_name"],
        r_output_attrs=["sponsor", "parent"],
        show_progress=False,
    )
    """

    # Rule-based blocker
    atypes1 = em.get_attr_types(A)
    atypes2 = em.get_attr_types(B)
    block_c = em.get_attr_corres(A, B)
    block_t = em.get_tokenizers_for_blocking()
    block_s = em.get_sim_funs_for_blocking()
    block_f = em.get_features(A, B, atypes1, atypes2, block_c, block_t, block_s)

    ###### RULE 1
    a_name = "company_name"
    b_name = "sponsor"

    # rule 1 : company_name and sponsor should have at least 3 letters in common
    def my_feature_function(ltuple, rtuple):
        if type(rtuple) != str or type(ltuple) != str:
            return 0
        common_letters = Counter(ltuple) & Counter(rtuple)
        return sum((common_letters.values()))

    # Register this function in the block sim_funs.
    block_s["my_feature_function"] = my_feature_function

    # Creating a feature from the registered function.
    my_feature = em.get_feature_fn(
        f"my_feature_function(ltuple['{a_name}'], rtuple['{b_name}'])", block_t, block_s
    )
    # Adding the feature to the features block.
    em.add_feature(block_f, f"{a_name}_{b_name}_my_feature_function", my_feature)

    # Defining a rule base blocker.
    rb = em.RuleBasedBlocker()

    # Creating a ruleK.
    rule1 = ["company_name_sponsor_my_feature_function(ltuple, rtuple) > 2"]

    # Adding the rule to the blocker.
    rb.add_rule(rule1, block_f)

    ######### RULE 2
    c_name = "parent_company_name"
    d_name = "parent"

    # rule 2 : parent_company_name and parent should have at least 3 letters in common

    # Register this function in the block sim_funs.
    block_s["my_feature_function"] = my_feature_function

    # Creating a feature from the registered function.
    my_feature = em.get_feature_fn(
        f"my_feature_function(ltuple['{c_name}'], rtuple['{d_name}'])", block_t, block_s
    )
    # Adding the feature to the features block.
    em.add_feature(block_f, f"{c_name}_{d_name}_my_feature_function", my_feature)

    # Defining a rule base blocker.
    rb = em.RuleBasedBlocker()

    # Creating a ruleK.
    rule2 = ["parent_company_name_parent_my_feature_function(ltuple, rtuple) > 2"]

    # Adding the rule to the blocker.
    rb.add_rule(rule2, block_f)

    def my_feature_function2(ltuple, rtuple):
        if type(rtuple) != str or type(ltuple) != str:
            return 0
        return fuzz.ratio(rtuple, ltuple)

    ########### RULE 3
    # rule 3 : company_name and sponsor should have a high fuzzy score

    e_name = "company_name"
    f_name = "sponsor"

    # Register this function in the block sim_funs.
    block_s["my_feature_function2"] = my_feature_function2

    # Creating a feature from the registered function.
    my_feature = em.get_feature_fn(
        f"my_feature_function2(ltuple['{e_name}'], rtuple['{f_name}'])",
        block_t,
        block_s,
    )
    # Adding the feature to the features block.
    em.add_feature(block_f, f"{e_name}_{f_name}_my_feature_function2", my_feature)

    # Defining a rule base blocker.
    rb = em.RuleBasedBlocker()

    # Creating a ruleK.
    rule3 = ["company_name_sponsor_my_feature_function2(ltuple, rtuple) > 98"]

    # Adding the rule to the blocker.
    rb.add_rule(rule3, block_f)

    ########### RULE 4
    # rule 4 : parent_company_name and parent should have a high fuzzy score

    g_name = "parent_company_name"
    h_name = "parent"

    # Register this function in the block sim_funs.
    block_s["my_feature_function2"] = my_feature_function2

    # Creating a feature from the registered function.
    my_feature = em.get_feature_fn(
        f"my_feature_function2(ltuple['{g_name}'], rtuple['{h_name}'])",
        block_t,
        block_s,
    )
    # Adding the feature to the features block.
    em.add_feature(block_f, f"{g_name}_{h_name}_my_feature_function2", my_feature)

    # Defining a rule base blocker.
    rb = em.RuleBasedBlocker()

    # Creating a ruleK.
    rule4 = ["parent_company_name_parent_my_feature_function2(ltuple, rtuple) > 98"]

    # Adding the rule to the blocker.
    rb.add_rule(rule4, block_f)

    # Generating the candidates set (here it may crash).
    C = rb.block_tables(
        A,
        B,
        l_output_attrs=["company_name", "parent_company_name"],
        r_output_attrs=["sponsor", "parent"],
    )

    print("nb of tuples after blocking")
    print(len(C))

    # MATCH TUPLE PAIRS

    # Sample  candidate set
    # Label G
    # specify 1 for matches and 0 for no matches


    # Uncomment the following code if you want to label the data again
    # If not, the already labeled data will be loaded in G
    """
    # G = em.sample_table(C, 681)

    ### CAREFUL !!!! MODIFICATION HAS PROBABLY TO BE DONE DEPENDING ON YOUR PANDAS VERSION
    # in p-le-projet-ia-carbometrix/env/lib/python3.8/site-packages/py_entitymatching/gui/table_gui.py
    # replace line 97 by
    # table.at[idxv[i], cols[j]] = val

    G = em.label_table(G, "gold", True)

    G.to_csv("sampled_data.csv", index=False)
    em.to_csv_metadata(G, "sampled_data.metadata")
    """

    G = pd.read_csv("sampled_data.csv")
    # em.to_csv_metadata(G, "sampled_data.metadata")
    em.set_fk_ltable(G, "ltable_id")
    em.set_fk_rtable(G, "rtable_id")
    em.set_key(G, "_id")
    em.set_rtable(G, B)
    em.set_ltable(G, A)
    # print(G.head())

    ## PREPARE FOR TRAINING

    # Split G into development set (I) and evaluation set (J)
    IJ = em.split_train_test(G, train_proportion=0.7, random_state=0)
    I = IJ["train"]
    J = IJ["test"]

    # Generate features automatically
    feature_table = em.get_features_for_matching(
        A, B, validate_inferred_attr_types=False
    )

    # use the following line to find how to complete attrs_from_table
    # print(G.head())

    # Select the attrs. to be included in the feature vector table
    attrs_from_table = [
        "ltable_company_name",
        "ltable_parent_company_name",
        "rtable_sponsor",
        "rtable_parent",
    ]

    ## TRAINING

    # Convert the labeled data to feature vectors using the feature table
    H = em.extract_feature_vecs(
        G,
        feature_table=feature_table,
        attrs_before=attrs_from_table,
        attrs_after="gold",
        show_progress=False,
    )

    # HERE WE SHOULD THINK LATER ON PROCESSING MISSING VALUES, LOOK AT THE DOCUMENTATION
    # http://anhaidgroup.github.io/py_entitymatching/v0.4.0/user_manual/imputing_missing_values.html

    # Instantiate the RF Matcher
    rf = em.RFMatcher()

    # HERE WE COULD ADD A RULE-BASED MATCHER

    # Get the attributes to be projected while training
    attrs_to_be_excluded = []
    attrs_to_be_excluded.extend(["_id", "ltable_id", "rtable_id", "gold"])
    attrs_to_be_excluded.extend(attrs_from_table)

    # Train using feature vectors from the labeled data.
    rf.fit(table=H, exclude_attrs=attrs_to_be_excluded, target_attr="gold")

    # Save the model
    pickle.dump(rf, open("model2", "wb"))

    t1_stop = process_time()
    print("process time :")
    print(t1_stop - t1_start)
