"""
This file is used to predict the output on dataset_with_150_matching.csv,
matching the columns lei_child and wri_eia_owner
It is used in out_analysis to evaluate the model
The current blocker is the overlap blocker but it can be modified
You can choose the model
"""

import sys

sys.path.append(
    "/Users/pradap/Documents/Research/Python-Package/anhaid/py_entitymatching/"
)

import py_entitymatching as em
import pandas as pd
import os
import PyQt5
import random
import pickle

if __name__ == "__main__":
    pd.set_option("display.max_columns", None)

    rf = pickle.load(open("model2", "rb"))

    ### GENERATE OTHER PREDICTIONS TO CHECK THE MODEL
    D = pd.read_csv("dataset_with_150_matching.csv")
    A = D[["lei_child"]]
    B = D[["wri_eia_owner"]]
    A["id"] = A.index
    B["id"] = B.index
    em.set_key(A, "id")
    em.set_key(B, "id")
    em.to_csv_metadata(A, "lei_child.metadata")
    em.to_csv_metadata(B, "wri_eia_owner.metadata")

    ob = em.OverlapBlocker()
    C = ob.block_tables(
        A,
        B,
        "lei_child",
        "wri_eia_owner",
        overlap_size=1,
        l_output_attrs=["lei_child"],
        r_output_attrs=["wri_eia_owner"],
        show_progress=False,
    )

    print(C.head())

    # Generate features automatically
    feature_table = em.get_features_for_matching(
        A, B, validate_inferred_attr_types=False
    )

    # use the following line to find how to complete attrs_from_table
    # print(G.head())

    # Select the attrs. to be included in the feature vector table
    attrs_from_table = ["ltable_lei_child", "rtable_wri_eia_owner"]

    ## PREDICTING

    # Convert the cancidate set to feature vectors using the feature table
    L = em.extract_feature_vecs(
        C,
        feature_table=feature_table,
        attrs_before=attrs_from_table,
        show_progress=False,
        n_jobs=-1,
    )

    # Get the attributes to be excluded while predicting
    attrs_to_be_excluded = []
    attrs_to_be_excluded.extend(["_id", "ltable_id", "rtable_id"])
    attrs_to_be_excluded.extend(attrs_from_table)

    # Predict the matches
    predictions = rf.predict(
        table=L,
        exclude_attrs=attrs_to_be_excluded,
        append=True,
        target_attr="predicted",
        inplace=False,
    )

    # Get the attributes to be projected out
    attrs_proj = []
    attrs_proj.extend(["_id", "ltable_id", "rtable_id"])
    attrs_proj.extend(attrs_from_table)
    attrs_proj.append("predicted")

    # Project the attributes
    predictions = predictions[attrs_proj]

    predictions.to_csv("check2.csv", index=False)
    print(predictions.head())