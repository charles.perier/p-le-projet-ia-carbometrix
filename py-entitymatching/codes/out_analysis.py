"""
This file is used to evaluate a model
There are two steps :
- check the output of 900_ref x coal-tracker, print the 
summary generated by py-entitymatching
- check how the model works on other tables
here it is dataset_with_150_matching

The file 10_000_1000_first_lines can be used to perform tests
on more tables, but be careful of the tables length
"""

import sys

sys.path.append(
    "/Users/pradap/Documents/Research/Python-Package/anhaid/py_entitymatching/"
)

import py_entitymatching as em
import pandas as pd
import os
import PyQt5


if __name__ == "__main__":
    pd.set_option("display.max_columns", None)

    # reading the csv files
    path_A = (
        em.get_install_path()
        + os.sep
        + "datasets"
        + os.sep
        + "end-to-end"
        + os.sep
        + "../../../../../../../data/900_ref.csv"
    )
    path_B = (
        em.get_install_path()
        + os.sep
        + "datasets"
        + os.sep
        + "end-to-end"
        + os.sep
        + "../../../../../../../data/coal-tracker.csv"
    )
    A = pd.read_csv(path_A)
    B = pd.read_csv(path_B)
    A["id"] = A.index
    em.set_key(A, "id")
    em.set_key(B, "id")
    em.to_csv_metadata(A, path_A)
    em.to_csv_metadata(B, path_B)

    G = pd.read_csv("sampled_data.csv")
    em.set_fk_ltable(G, "ltable_id")
    em.set_fk_rtable(G, "rtable_id")
    em.set_key(G, "_id")
    em.set_rtable(G, B)
    em.set_ltable(G, A)

    pred = pd.read_csv("out2.csv")
    em.set_fk_ltable(pred, "ltable_id")
    em.set_fk_rtable(pred, "rtable_id")
    em.set_key(pred, "_id")
    em.set_rtable(G, B)
    em.set_ltable(G, A)

    # CHECK NUMBER OF MATCHES
    print("\n")
    print("*** positives in sample ***")
    count = G["gold"].value_counts()
    print(count)

    print("*** positives in predictions ***")
    count = pred["predicted"].value_counts()
    print(count)

    # CHECK WITH LABELED VALUES
    # add column predicted to sampled
    print("\n")
    print("*** em.eval_summary with labelled ***")
    H = pd.merge(left=G, right=pred, on=["ltable_id", "rtable_id"], how="left")
    em.set_fk_ltable(H, "ltable_id")
    em.set_fk_rtable(H, "rtable_id")
    em.set_key(H, "_id_x")
    em.set_rtable(H, B)
    em.set_ltable(H, A)
    eval_summary = em.eval_matches(H, "gold", "predicted")
    em.print_eval_summary(eval_summary)

    # CHECK WITH OUTER SAMPLED VALUES (dataset with 150 matching)
    print("\n")
    print("*** em.eval_summary with 150 labelled ***")

    D = pd.read_csv("dataset_with_150_matching.csv")
    D = D[~D["lei_child"].isna()]
    A = D[["lei_child"]]

    B = D[["wri_eia_owner"]]
    A["id"] = A.index
    B["id"] = B.index
    em.set_key(A, "id")
    em.set_key(B, "id")
    em.to_csv_metadata(A, "lei_child.metadata")
    em.to_csv_metadata(B, "wri_eia_owner.metadata")
    # print("TAILLE")

    # print(D.shape[0])
    I = pd.read_csv("check.csv")
    em.set_fk_ltable(I, "ltable_id")
    em.set_fk_rtable(I, "rtable_id")
    em.set_key(I, "_id")
    em.set_rtable(I, B)
    em.set_ltable(I, A)
    I.rename(
        columns={
            "ltable_lei_child": "lei_child",
            "rtable_wri_eia_owner": "wri_eia_owner",
        },
        inplace=True,
    )

    em.set_fk_ltable(D, "lei_child")
    em.set_fk_rtable(D, "wri_eia_owner")
    D["id"] = D.index
    em.set_key(D, "id")
    em.set_rtable(D, B)
    em.set_ltable(D, A)
    t = D.shape[0]
    D["gold"] = [1 for _ in range(t)]

    T = pd.merge(left=D, right=I, on=["lei_child", "wri_eia_owner"], how="left")
    T["_id"] = T.index
    em.set_fk_ltable(T, "lei_child")
    em.set_fk_rtable(T, "wri_eia_owner")
    em.set_key(T, "_id")
    em.set_rtable(T, B)
    em.set_ltable(T, A)

    eval_summary = em.eval_matches(T, "gold", "predicted")
    em.print_eval_summary(eval_summary)

    # print some false_negatives

    fp = T[T["predicted"] == 0]
