# Pôle Projet IA - carbometrix

Projet 1.04 du pôle IA en collaboration avec carbometrix

Team members :
Inés Dormoy
Kiria Filleul
Zoé Garbal
Mohamed Iyadh Laouej
Charles Perier

### Create a virtualenv

***For Windows***  

- Open a terminal anaconda prompt
- Type "pip install virtualenv"
- Type "virtualenv -p <desired python version> env"
- Activate the virtualenv with "source env/Scripts/activate" on Windows or "source env/bin/activate" on Mac and Linux 

***For Linux/Mac***  
```bash  
virtualenv env -p python3

. env/bin/activate (deactivate when you´re finished or to get out of your environment)

```
### Install requirements

```bash  
pip install -r requirements.txt

```

### Install py-entitymatching 
This module has to be installed appart from the others.
Follow the instructions from the documentation.
